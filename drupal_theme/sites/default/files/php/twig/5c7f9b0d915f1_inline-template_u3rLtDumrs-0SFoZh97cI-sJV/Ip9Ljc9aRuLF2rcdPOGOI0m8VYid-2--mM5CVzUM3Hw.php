<?php

/* {# inline_template_start #}<div class="panel panel-default">
<div class="panel-heading">
<div class="col-sm-4">
<img class="img-responsive" src="{{ field_image }}" alt="Card image" />
</div>
<div class="panel-body">
<h4>{{ title }}</h4>
<div class="col-sm-8">
<div><b>Description : </b> {{ body_1 }}</div>
<div><b>Department : </b>  {{ field_department }} </div>
<div> <b>Branch : </b>{{ field_branch }}</div>
<div> <b>Year : </b>{{ field_year }} </div>
<div><img class="img-responsive" src="{{ field_image_1 }}" alt="Card image" /></div>
</div>
</div>
</div>
</div>
<br /> */
class __TwigTemplate_a798b849e43d264325cf8b2243cded7c4f05e2cbc1b326231be8ce08354a02f9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<div class=\"panel panel-default\">
<div class=\"panel-heading\">
<div class=\"col-sm-4\">
<img class=\"img-responsive\" src=\"";
        // line 4
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_image"] ?? null), "html", null, true));
        echo "\" alt=\"Card image\" />
</div>
<div class=\"panel-body\">
<h4>";
        // line 7
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title"] ?? null), "html", null, true));
        echo "</h4>
<div class=\"col-sm-8\">
<div><b>Description : </b> ";
        // line 9
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["body_1"] ?? null), "html", null, true));
        echo "</div>
<div><b>Department : </b>  ";
        // line 10
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_department"] ?? null), "html", null, true));
        echo " </div>
<div> <b>Branch : </b>";
        // line 11
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_branch"] ?? null), "html", null, true));
        echo "</div>
<div> <b>Year : </b>";
        // line 12
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_year"] ?? null), "html", null, true));
        echo " </div>
<div><img class=\"img-responsive\" src=\"";
        // line 13
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_image_1"] ?? null), "html", null, true));
        echo "\" alt=\"Card image\" /></div>
</div>
</div>
</div>
</div>
<br />";
    }

    public function getTemplateName()
    {
        return "{# inline_template_start #}<div class=\"panel panel-default\">
<div class=\"panel-heading\">
<div class=\"col-sm-4\">
<img class=\"img-responsive\" src=\"{{ field_image }}\" alt=\"Card image\" />
</div>
<div class=\"panel-body\">
<h4>{{ title }}</h4>
<div class=\"col-sm-8\">
<div><b>Description : </b> {{ body_1 }}</div>
<div><b>Department : </b>  {{ field_department }} </div>
<div> <b>Branch : </b>{{ field_branch }}</div>
<div> <b>Year : </b>{{ field_year }} </div>
<div><img class=\"img-responsive\" src=\"{{ field_image_1 }}\" alt=\"Card image\" /></div>
</div>
</div>
</div>
</div>
<br />";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 13,  88 => 12,  84 => 11,  80 => 10,  76 => 9,  71 => 7,  65 => 4,  60 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# inline_template_start #}<div class=\"panel panel-default\">
<div class=\"panel-heading\">
<div class=\"col-sm-4\">
<img class=\"img-responsive\" src=\"{{ field_image }}\" alt=\"Card image\" />
</div>
<div class=\"panel-body\">
<h4>{{ title }}</h4>
<div class=\"col-sm-8\">
<div><b>Description : </b> {{ body_1 }}</div>
<div><b>Department : </b>  {{ field_department }} </div>
<div> <b>Branch : </b>{{ field_branch }}</div>
<div> <b>Year : </b>{{ field_year }} </div>
<div><img class=\"img-responsive\" src=\"{{ field_image_1 }}\" alt=\"Card image\" /></div>
</div>
</div>
</div>
</div>
<br />", "{# inline_template_start #}<div class=\"panel panel-default\">
<div class=\"panel-heading\">
<div class=\"col-sm-4\">
<img class=\"img-responsive\" src=\"{{ field_image }}\" alt=\"Card image\" />
</div>
<div class=\"panel-body\">
<h4>{{ title }}</h4>
<div class=\"col-sm-8\">
<div><b>Description : </b> {{ body_1 }}</div>
<div><b>Department : </b>  {{ field_department }} </div>
<div> <b>Branch : </b>{{ field_branch }}</div>
<div> <b>Year : </b>{{ field_year }} </div>
<div><img class=\"img-responsive\" src=\"{{ field_image_1 }}\" alt=\"Card image\" /></div>
</div>
</div>
</div>
</div>
<br />", "");
    }
}
