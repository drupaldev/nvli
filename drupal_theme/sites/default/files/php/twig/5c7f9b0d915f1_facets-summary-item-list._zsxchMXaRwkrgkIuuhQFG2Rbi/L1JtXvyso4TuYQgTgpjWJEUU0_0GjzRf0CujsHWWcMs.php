<?php

/* modules/contrib/facets/modules/facets_summary/templates/facets-summary-item-list.html.twig */
class __TwigTemplate_664d99a373b7929a22d514b5190d5f1c53b703a81807ac16c13638a5ccfd82a1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("if" => 24, "set" => 25, "for" => 48);
        $filters = array("length" => 34, "raw" => 58);
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('if', 'set', 'for'),
                array('length', 'raw'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 24
        if ($this->getAttribute(($context["context"] ?? null), "list_style", array())) {
            // line 25
            $context["attributes"] = $this->getAttribute(($context["attributes"] ?? null), "addClass", array(0 => ("item-list__" . $this->getAttribute(($context["context"] ?? null), "list_style", array()))), "method");
        }
        // line 28
        if ((($context["items"] ?? null) || ($context["empty"] ?? null))) {
            // line 30
            if ( !twig_test_empty(($context["title"] ?? null))) {
                // line 31
                echo "<h3>";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["title"] ?? null), "html", null, true));
                echo "</h3>";
            }
            // line 34
            if (((twig_length_filter($this->env, ($context["items"] ?? null)) >= 1) || (twig_length_filter($this->env, ($context["search_query"] ?? null)) > 0))) {
                // line 35
                echo "<span class=\"selected_filters_txt\">Selected Filters: </span>
   <span class=\"selected_filters_tabs\">




    <";
                // line 41
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["list_type"] ?? null), "html", null, true));
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["attributes"] ?? null), "html", null, true));
                echo ">";
                // line 43
                if ((twig_length_filter($this->env, ($context["search_query"] ?? null)) > 0)) {
                    // line 44
                    echo "<li><a href=\"";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["newlink"] ?? null), "html", null, true));
                    echo "\"> ";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["search_query"] ?? null), "html", null, true));
                    echo "</a></li>";
                }
                // line 48
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    // line 49
                    echo "<li";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($context["item"], "attributes", array()), "html", null, true));
                    echo ">";
                    echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($context["item"], "value", array()), "html", null, true));
                    echo "</li>";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 52
                echo "<li><a href=\"/drupal_theme/nai-records-list\" rel=\"nofollow\">Reset All</a></li>
    </";
                // line 53
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["list_type"] ?? null), "html", null, true));
                echo ">
  </span>";
            } else {
                // line 56
                echo "<span class=\"selected_filters_txt\">Selected Filters: </span>
    <span class=\"default_filters_tabs\">   
     ";
                // line 58
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(($context["empty"] ?? null)));
                echo "
    </span>
  </span>";
            }
        }
    }

    public function getTemplateName()
    {
        return "modules/contrib/facets/modules/facets_summary/templates/facets-summary-item-list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  106 => 58,  102 => 56,  97 => 53,  94 => 52,  84 => 49,  80 => 48,  73 => 44,  71 => 43,  67 => 41,  59 => 35,  57 => 34,  52 => 31,  50 => 30,  48 => 28,  45 => 25,  43 => 24,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#
/**
 * @file
 * Default theme implementation for a facets summary item list.
 *
 * Available variables:
 * - items: A list of items. Each item contains:
 *   - attributes: HTML attributes to be applied to each list item.
 *   - value: The content of the list element.
 * - title: The title of the list.
 * - list_type: The tag for list element (\"ul\" or \"ol\").
 * - wrapper_attributes: HTML attributes to be applied to the list wrapper.
 * - attributes: HTML attributes to be applied to the list.
 * - empty: A message to display when there are no items. Allowed value is a
 *   string or render array.
 * - context: A list of contextual data associated with the list. May contain:
 *   - list_style: The custom list style.
 *
 * @see facets_summary_preprocess_facets_summary_item_list()
 *
 * @ingroup themeable
 */
#}
{% if context.list_style %}
  {%- set attributes = attributes.addClass('item-list__' ~ context.list_style) %}
{% endif %}

{%- if items or empty -%}

  {%- if title is not empty -%}
    <h3>{{ title }}</h3>
  {%- endif -%}
   
  {%- if items|length >= 1 or search_query|length > 0 -%}
   <span class=\"selected_filters_txt\">Selected Filters: </span>
   <span class=\"selected_filters_tabs\">




    <{{ list_type }}{{ attributes }}>
 
      {%- if search_query|length > 0 -%}
        <li><a href=\"{{ newlink }}\"> {{ search_query }}</a></li>
      {%- endif -%}


      {%- for item in items -%}
        <li{{ item.attributes }}>{{ item.value }}</li>
      {%- endfor -%}

       <li><a href=\"/drupal_theme/nai-records-list\" rel=\"nofollow\">Reset All</a></li>
    </{{ list_type }}>
  </span>
  {%- else -%}
  <span class=\"selected_filters_txt\">Selected Filters: </span>
    <span class=\"default_filters_tabs\">   
     {{ empty|raw }}
    </span>
  </span>
  {%- endif -%}

{%- endif -%}



", "modules/contrib/facets/modules/facets_summary/templates/facets-summary-item-list.html.twig", "/var/www/html/drupal_theme/modules/contrib/facets/modules/facets_summary/templates/facets-summary-item-list.html.twig");
    }
}
